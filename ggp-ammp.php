<?php
/*
Plugin Name: GG Another Maintenance Mode Plugin
Plugin URI: https://bitbucket.org/ggoyanes/gg-another-maintenance-plugin
Description: A maintenance mode plugin that allows you to use an external PHP file as maintenance page.
Version: 1.0.20180722
Author: Guillermo Goyanes
Author URI: mailto:ggoyanes.dev@gmail.com
License: GPL2
*/
